<?php
// Color system
$colorBrandGrey = 'rgb(51, 62, 72)';
$colorBrandGrey100= $colorBrandGrey;
$colorBrandGrey80 = 'rgb(92, 101, 109)';
$colorBrandGrey60 = 'rgb(132, 139, 145)';
$colorBrandGrey33 = 'rgb(188, 191, 194)';
$colorBrandGrey10 = 'rgb(235, 236, 237)';

$colorBrandAquamarine = 'rgb(70, 245, 170)';
$colorBrandBlue = 'rgb(37, 117, 252)';
$colorBrandFrog = 'rgb(0, 255, 0)';
$colorBrandGreen = 'rgb(0, 189, 107)';
$colorBrandLightblue = 'rgb(0, 236, 188)';
$colorBrandMint = 'rgb(130, 255, 136)';
$colorBrandMistblue = 'rgb(109, 148, 255)';
$colorBrandNeon = 'rgb(216, 255, 0)';
$colorBrandOrange = 'rgb(255, 151, 0)';
$colorBrandPetrol = 'rgb(18, 99, 119)';
$colorBrandPineapple = 'rgb(232, 255, 102)';
$colorBrandPurple = 'rgb(106, 17, 203)';
$colorBrandRed = 'rgb(255, 8, 68)';
$colorBrandSun = 'rgb(255, 228, 109)';
$colorBrandViolette = 'rgb(138, 51, 126)';
$colorBrandViolet = $colorBrandViolette;
$colorBrandYellow = 'rgb(255, 239, 58)';
$colorBrandYellowForum = 'rgb(245, 255, 0)';

$colorBlack = 'rgb(0, 0, 0)';
$colorWhite = 'rgb(255, 255, 255)';

$brandColors = [
    'aquamarine' => $colorAquamarine,
    'blue' => $colorBrandBlue,
    'frog' => $colorBrandFrog,
    'green' => $colorBrandGreen,
    'grey'=> $colorBrandGrey,
    'grey-10'=> $colorBrandGrey10,
    'lightblue' => $colorBrandLightblue,
    'mint' => $colorBrandMint,
    'mistblue' => $colorBrandMistblue,
    'neon' => $colorBrandNeon,
    'orange' => $colorBrandOrange,
    'petrol' => $colorBrandPetrol,
    'pineapple' => $colorPineapple,
    'purple' => $colorBrandPurple,
    'red' => $colorBrandRed,
    'sun' => $colorSun,
    'violet' => $colorBrandViolet,
    'yellow' => $colorBrandYellow,
    'yellow-forum' => $colorBrandYellowForum,
    'white' => $colorWhite,
];
