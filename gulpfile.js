const { dest, series, src, watch } = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const sassLint = require('gulp-sass-lint');
const sourcemaps = require('gulp-sourcemaps');

const distDir = 'dist/';

const fontDir = 'fonts';
const fontFilesInclude = [
  'node_modules/@fortawesome/fontawesome-free/webfonts/*',
];

const sassFiles = 'scss/**/*.scss';

const autoprefixerOptions = {
  grid: true,
};

function fontTask() {
  return src( fontFilesInclude ).pipe( dest( fontDir ) );
}

function cssTask() {
  return src('scss/style.scss')
    .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer(autoprefixerOptions))
      .pipe(cleanCSS())
      .pipe(rename({
        suffix: '.min'
      }))
    .pipe(sourcemaps.write('./'))
    .pipe(dest(distDir));
}

function testTask() {
  return src(sassFiles)
    .pipe(sassLint())
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError());
}

function watchTask() {
  watch(
    sassFiles,
    series(
      testTask,
      cssTask
    )
  );
}

const buildTask = series(
  fontTask,
  testTask,
  cssTask
);

exports.fonts = cssTask;
exports.css = cssTask;
exports.test = testTask;
exports.watch = watchTask;
exports.default = buildTask;
