# d.velop brand

The d.velop brand assets for web projects.

## Development Setup

### Technical requirements

- Node.js
- npm
- Gulp.js

### Installation

```bash
$ npm install
$ gulp
```

## File watcher

```bash
$ gulp watch
```

## Testing

```bash
$ gulp test
```
